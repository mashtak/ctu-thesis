import com.google.gson.Gson;
import cz.mashtak.dto.Record;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParseJsonTest {
    @Test
    public void testParse(){
        Gson gson = new Gson();
        try {
            Reader reader = new FileReader("src/test/resources/message.json");
            Record record = gson.fromJson(reader,Record.class);
            System.out.println(record.toString());
            assertEquals(record.getLog(), "Hello Fluentd!");
            assertEquals(record.getTag(), "metronome.metest");
            assertEquals(record.getTimestamp(), "2019-04-22T18:45:48.05778+02:00");
            assertEquals(record.getSource(), Record.STDOUT);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
