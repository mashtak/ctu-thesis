package cz.mashtak;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.cql.BoundStatement;
import com.datastax.oss.driver.api.core.cql.PreparedStatement;
import cz.mashtak.dto.Record;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.InetSocketAddress;
import java.util.Collection;

public class CassandraConnector {

    final static Logger LOG = LogManager.getLogger(CassandraConnector.class.getName());
    private CqlSession session;
    private PreparedStatement insertLogMessageQueryStdout;
    private PreparedStatement insertLogMessageQueryStderr;

    public CassandraConnector(Collection<InetSocketAddress> contactPoints, String datacenter) {

        this.session = CqlSession.builder()
                .withLocalDatacenter(datacenter)
                .addContactPoints(contactPoints)
                .build();
        this.insertLogMessageQueryStdout = session.prepare("INSERT INTO runs.stdout (source,timestamp,log) VALUES (?,?,?)");
        this.insertLogMessageQueryStderr = session.prepare("INSERT INTO runs.stderr (source,timestamp,log) VALUES (?,?,?)");
        LOG.info("Connected to cassandra database ");
    }

    public void insertLogMessageStdout(Record record) {
        BoundStatement bound = insertLogMessageQueryStdout.bind(record.getTag(),record.getTimestamp(),record.getLog());
        session.execute(bound);
    }

    public void insertLogMessageStderr(Record record) {
        BoundStatement bound = insertLogMessageQueryStderr.bind(record.getTag(),record.getTimestamp(),record.getLog());
        session.execute(bound);
    }

    public void finalize() {
        this.session.close();
    }
}
