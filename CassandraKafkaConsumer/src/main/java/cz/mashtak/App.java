package cz.mashtak;

import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.InetSocketAddress;
import java.util.*;
import java.util.stream.Collectors;

public class App {
    final static Logger LOG = LogManager.getLogger(App.class.getName());

    public static void main(String[] args) {
        Options options = new Options();

        Option kafkaTopicInput = new Option("kafkaTopic", true, "Kafka Topic to be consumed");
        kafkaTopicInput.setRequired(true);
        options.addOption(kafkaTopicInput);

        Option kafkaBrokersInput = new Option("kafkaBroker", true, "address:port,address:port of the kafka brokers");
        kafkaBrokersInput.setRequired(true);
        options.addOption(kafkaBrokersInput);

        Option kafkaGroupId = new Option("kafkaGroupId", true, "Kafka GroupId");
        kafkaGroupId.setRequired(true);
        options.addOption(kafkaGroupId);

        Option cassandraContactPointsInput = new Option("cassandraContactPoints", true, "Cassandra contact points in format address:port,address:port,...");
        cassandraContactPointsInput.setRequired(true);
        options.addOption(cassandraContactPointsInput);

        Option cassandraDatacenterInput = new Option("cassandraDatacenter", true, "Name of the Cassandra Datacenter");
        cassandraDatacenterInput.setRequired(true);
        options.addOption(cassandraDatacenterInput);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();

        CommandLine cmd=null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("Cassandra kafka consumer", options);

            System.exit(1);
        }


        String kafkaBrokers = cmd.getOptionValue(kafkaBrokersInput.getOpt());
        String topic = cmd.getOptionValue(kafkaTopicInput.getOpt());
        String groupId = cmd.getOptionValue(kafkaGroupId.getOpt());
        String cassandraDatacenter = cmd.getOptionValue(cassandraDatacenterInput.getOpt());
        String cassandraContactPointsString = cmd.getOptionValue(cassandraContactPointsInput.getOpt());
        LOG.info("Kafka consumer started with parameters \n\t Kafka:" + kafkaBrokers + " topic:" + topic + " groupId:" + groupId + "\n\t Cassandra: " + cassandraContactPointsString+" datacenter:"+cassandraDatacenter);

        Collection<InetSocketAddress> cassandraContactPoints = parseAddresses(cassandraContactPointsString);

//        int numConsumers = 1;
//        ExecutorService executor = Executors.newFixedThreadPool(numConsumers);


        CassandraConnector cassandraConnector = new CassandraConnector(cassandraContactPoints,cassandraDatacenter);
        KafkaConsumer consumer = new KafkaConsumer(kafkaBrokers, topic, groupId, cassandraConnector);
        consumer.run();


//        final List<cz.mashtak.KafkaConsumer> consumers = new ArrayList<>();
//        for (int i = 0; i < numConsumers; i++) {
//            cz.mashtak.KafkaConsumer consumer = new cz.mashtak.KafkaConsumer(kafkaBrokers, topic, groupId, cassandraConnector,executor);
//
//            consumers.add(consumer);
//            executor.submit(consumer);
//        }
//
//        Runtime.getRuntime().addShutdownHook(new Thread() {
//            @Override
//            public void run() {
//                for (cz.mashtak.KafkaConsumer consumer : consumers) {
//                    consumer.shutdown();
//                }
//                executor.shutdown();
//                try {
//                    executor.awaitTermination(5000, TimeUnit.MILLISECONDS);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
    }

    private static Collection<InetSocketAddress> parseAddresses(String cassandraContactPoints) {
        return Arrays.stream(cassandraContactPoints.split(",")).map(address -> {
            String[] addr = address.split(":");
            return new InetSocketAddress(addr[0],Integer.parseInt(addr[1]));
        }).collect(Collectors.toCollection(LinkedList::new));
    }


}
