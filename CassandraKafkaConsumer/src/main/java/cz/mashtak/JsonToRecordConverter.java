package cz.mashtak;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import cz.mashtak.dto.Record;

public class JsonToRecordConverter {
    static Gson gson = new Gson();
    public static Record convertJson(String json) throws JsonSyntaxException {
        Record record = gson.fromJson(json,Record.class);
        return record;
    }
}
