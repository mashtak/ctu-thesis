package cz.mashtak.dto;

public class Record {
    public final static String STDERR = "stderr";
    public final static String STDOUT = "stdout";
    private String timestamp;
    private String tag;
    private String log;
    private String source;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    @Override
    public String toString() {
        return "Record{" +
                "timestamp='" + timestamp + '\'' +
                ", tag='" + tag + '\'' +
                ", log='" + log + '\'' +
                '}';
    }
}
