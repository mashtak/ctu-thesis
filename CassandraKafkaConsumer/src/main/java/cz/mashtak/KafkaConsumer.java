package cz.mashtak;

import com.google.gson.JsonSyntaxException;
import cz.mashtak.dto.Record;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.consumer.OffsetCommitCallback;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class KafkaConsumer implements Runnable {

    final static Logger LOG = LogManager.getLogger(KafkaConsumer.class.getName());

    String kafkaBroker;
    String topic;
    String groupId;
    Consumer<Long, String> consumer;
    CassandraConnector cassandraConnector;
//    ExecutorService executorService;


    public KafkaConsumer(String kafkaBroker, String topic, String groupId, CassandraConnector cassandraConnector) {
        this.kafkaBroker = kafkaBroker;
        this.topic = topic;
        this.groupId = groupId;
        this.consumer = ConsumerFactory.createConsumer(kafkaBroker, topic, groupId);
        this.cassandraConnector = cassandraConnector;
    }


    @Override
    public void run() {
        LOG.info("Consuming from kafka");
        try {
            while (true) {
                ConsumerRecords<Long, String> records = consumer.poll(Duration.ofDays(365));
                records.forEach(record -> {
                    LOG.debug(record.toString());
                    try{
                        Record messageRecord = JsonToRecordConverter.convertJson(record.value());
                        if (Record.STDOUT.equals(messageRecord.getSource())){
                            cassandraConnector.insertLogMessageStdout(messageRecord);
                        }
                        else if(Record.STDERR.equals(messageRecord.getSource())){
                            cassandraConnector.insertLogMessageStderr(messageRecord);
                        } else{
                            LOG.error("Unknown message source "+messageRecord.toString());
                        }
                    }catch (JsonSyntaxException e){
                        LOG.error("Malformed JSON: "+record.toString());
                    }
                    consumer.commitAsync(new OffsetCommitCallback() {
                        @Override
                        public void onComplete(Map<TopicPartition, OffsetAndMetadata> offsets,
                                               Exception exception) {
                            if (exception != null) {
                                LOG.error(exception);
                            }
                        }
                    });
                });
            }
        } catch (WakeupException e) {
            // ignore for shutdown
            LOG.info("Wakeup exception catched, ending");
        }catch (Exception e){
            LOG.error(e);
//            executorService.shutdown();
        } finally {
            consumer.close();
        }


    }

    public void shutdown() {
        consumer.wakeup();
    }

}
