#!/bin/bash
set -e -x

if [ $1 == "consumer" ]; then
  trap 'echo trap; kill -TERM $PID' TERM INT
  /usr/bin/java -jar /app.jar "$@" &
  PID=$!
  wait $PID
  trap - TERM INT
  wait $PID
  EXIT_STATUS=$?
else
  exec "$@"
fi