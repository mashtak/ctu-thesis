# metronome
A Docker image for [Metronome](https://github.com/dcos/metronome), the DC/OS job scheduler.

The code was taken from [mesoshq/metronome](https://github.com/mesoshq/metronome). The Dockerfile was changed to use up-to-date version of metronome