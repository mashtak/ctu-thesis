#!/bin/bash
set -e -x

if [ $1 == "server" ]; then
  trap 'echo trap; kill -TERM $PID' TERM INT
  /usr/bin/java -jar /app.jar --server.port=$PORT0 "$@" &
  PID=$!
  wait $PID
  trap - TERM INT
  wait $PID
  EXIT_STATUS=$?
else
  exec "$@"
fi