package cz.mashtak.tasksubmitter.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class TaskOutput {
    public final static String STDOUT = "stdout";
    public final static String STDERR = "stderr";
    @Getter @Setter
    private String[] ouput;

    @Getter @Setter
    private String user;

    @Getter @Setter
    private String name;

    @Getter @Setter
    private int runId;

    @Getter @Setter
    private String type;
}
