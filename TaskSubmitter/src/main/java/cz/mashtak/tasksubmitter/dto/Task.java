package cz.mashtak.tasksubmitter.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Service;

import java.time.Instant;

@ToString
public class Task {
    @Getter @Setter
    private String command;

    @Getter @Setter
    private String user;

    @Getter @Setter
    private String name;

    @Getter @Setter
    private String image;

    @Getter @Setter
    private Instant submissionTime;

    @Getter @Setter
    private float cpus;

    @Getter @Setter
    private int memory;

    @Getter @Setter
    private int disk;
}
