package cz.mashtak.tasksubmitter.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Instant;

@ToString
public class TaskRun {
    public final static String STATE_SUBMITTED="submitted";
    public final static String STATE_INITIAL="initial";
    public final static String STATE_ACTIVE="active";
    public final static String STATE_FINISHED="finished";

    @Getter @Setter
    private String user;

    @Getter @Setter
    private String name;

    @Getter @Setter
    private int runId;

    @Getter @Setter
    private Instant submissionTime;

    @Getter @Setter
    private String state;

    @Getter @Setter
    private Instant finishedAt;

}
