package cz.mashtak.tasksubmitter.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import cz.mashtak.tasksubmitter.client.MetronomeClient;
import cz.mashtak.tasksubmitter.dao.CassandraDAO;
import cz.mashtak.tasksubmitter.dto.Task;
import cz.mashtak.tasksubmitter.dto.TaskRun;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Slf4j
@Service
public class MetronomeService {
    @Autowired
    CassandraDAO cassandraDAO;

    @Autowired
    MetronomeClient metronomeClient;

    public TaskRun submitTaskRun(Task task, String runTaskId) {
        metronomeClient.submitTask(constructJson(task, runTaskId));
        metronomeClient.triggerTaskRun(runTaskId);
        TaskRun taskRun = new TaskRun();
        return taskRun;
    }

    public boolean updateTaskRunState(TaskRun taskRun, String metronomeId){
        String timeFinished = metronomeClient.checkTaskRunFinished(metronomeId);
        if(timeFinished != null){
//            remove quotas
            timeFinished = timeFinished.substring(1,timeFinished.length()-1);
            timeFinished = timeFinished.substring(0,timeFinished.indexOf("+"));
            taskRun.setFinishedAt(LocalDateTime.parse(timeFinished).toInstant(ZoneOffset.UTC));
            taskRun.setState(TaskRun.STATE_FINISHED);
            return true;
        }else {
            return false;
        }
    }

    public void deleteTaskRun(String metronomeId) {
        metronomeClient.deleteTaskRun(metronomeId);
    }
    private JsonObject constructJson(Task task, String metronomeId) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", metronomeId);
        JsonObject run = new JsonObject();
        run.addProperty("cpus", task.getCpus());
        run.addProperty("mem", task.getMemory());
        run.addProperty("disk", task.getDisk());
        run.addProperty("cmd", task.getCommand());
        JsonObject docker = new JsonObject();
        docker.addProperty("image", task.getImage());
        docker.add("parameters", getDockerParameters(metronomeId));
        run.add("docker", docker);
        jsonObject.add("run", run);
        return jsonObject;
    }

    private JsonArray getDockerParameters(String metronomeId) {
        JsonArray dockerParameters = new JsonArray();
        JsonObject param1 = new JsonObject();
        param1.addProperty("key", "log-driver");
        param1.addProperty("value", "fluentd");
        dockerParameters.add(param1);
        JsonObject param2 = new JsonObject();
        param2.addProperty("key", "log-opt");
        param2.addProperty("value", "tag="+metronomeId);
        dockerParameters.add(param2);
        return dockerParameters;
    }



}
