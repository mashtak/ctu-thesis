package cz.mashtak.tasksubmitter.service;

import cz.mashtak.tasksubmitter.dao.CassandraDAO;
import cz.mashtak.tasksubmitter.dto.TaskRun;
import cz.mashtak.tasksubmitter.dto.Task;
import cz.mashtak.tasksubmitter.dto.TaskOutput;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

@Slf4j
@Service
public class TaskService {
    @Autowired
    CassandraDAO cassandraDAO;

    @Autowired
    MetronomeService metronomeService;

    public Task submitTask(String user, Task task) {
        task.setUser(user);
        return cassandraDAO.submitTask(task);
    }

    public List<Task> getUserTasks(String user) {
        return cassandraDAO.getUserTasks(user);
    }

    public List<TaskRun> getTaskRuns(String user, String taskName) {
        List<TaskRun> taskRuns = cassandraDAO.getTaskRuns(user, taskName);
        updateTaskRunsStates(taskRuns);
        return taskRuns;
    }

    public TaskOutput getTaskRunStdout(String taskName, String user, int runId) {
        return cassandraDAO.getTaskRunStdout(prepareTaskOutput(taskName, user, runId), constructTaskRunId(taskName, user, runId));
    }

    public TaskOutput getTaskRunStderr(String taskName, String user, int runId) {
        return cassandraDAO.getTaskRunStderr(prepareTaskOutput(taskName, user, runId), constructTaskRunId(taskName, user, runId));
    }

    public TaskRun runTask(String user, String taskName) {
        Task task = cassandraDAO.getTask(user, taskName);
        int runId = cassandraDAO.nextAvailableRunId(user, taskName);
        TaskRun taskRun = metronomeService.submitTaskRun(task, constructTaskRunId(taskName, user, runId));
        taskRun.setUser(task.getUser());
        taskRun.setName(task.getName());
        taskRun.setRunId(runId);
        taskRun.setSubmissionTime(Instant.now());
        taskRun.setState(TaskRun.STATE_SUBMITTED);
        taskRun = cassandraDAO.insertTaskRun(taskRun);
        return taskRun;
    }

    private void updateTaskRunsStates(List<TaskRun> taskRuns) {
        taskRuns.stream().forEach(taskRun -> {
            if (TaskRun.STATE_FINISHED.equals(taskRun.getState())) {
                return;
            } else {
                updateTaskRunState(taskRun);
            }
        });
    }

    private void updateTaskRunState(TaskRun taskRun) {
        String metronomeId = constructTaskRunId(taskRun.getName(), taskRun.getUser(), taskRun.getRunId());
        boolean wasUpdated = metronomeService.updateTaskRunState(taskRun, metronomeId );
        if (wasUpdated) {
//            TODO only update state instead inserting whole object
            log.debug("Updating and removing taskrun from metronome"+metronomeId);
            cassandraDAO.insertTaskRun(taskRun);
            metronomeService.deleteTaskRun(metronomeId);
        }
    }

    private String constructTaskRunId(String taskName, String user, int runId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("metronome");
        stringBuilder.append(".");
        stringBuilder.append(user);
        stringBuilder.append(".");
        stringBuilder.append(taskName);
        stringBuilder.append(".");
        stringBuilder.append(runId);
        return stringBuilder.toString();
    }

    private TaskOutput prepareTaskOutput(String taskName, String user, int runId) {
        TaskOutput taskOutput = new TaskOutput();
        taskOutput.setRunId(runId);
        taskOutput.setName(taskName);
        taskOutput.setUser(user);
        return taskOutput;
    }
}