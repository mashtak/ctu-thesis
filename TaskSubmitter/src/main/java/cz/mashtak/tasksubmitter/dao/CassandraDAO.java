package cz.mashtak.tasksubmitter.dao;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.DefaultConsistencyLevel;
import com.datastax.oss.driver.api.core.cql.*;
import cz.mashtak.tasksubmitter.dto.Task;
import cz.mashtak.tasksubmitter.dto.TaskOutput;
import cz.mashtak.tasksubmitter.dto.TaskRun;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Repository;

import java.net.InetSocketAddress;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public class CassandraDAO {

    private CqlSession session;

    private PreparedStatement getLatestRunId;
    private PreparedStatement insertTask;
    private PreparedStatement insertRunTask;
    private PreparedStatement getTaskRunStdout;
    private PreparedStatement getTaskRunStderr;
    private PreparedStatement getTask;
    private PreparedStatement getTaskRuns;
    private PreparedStatement getUserTasks;


    public CassandraDAO(@Value("${cassandra.contact-points}") String contactPointsString,
                        @Value("${cassandra.datacenter}") String datacenter) {
        this.session = CqlSession.builder()
                .withLocalDatacenter(datacenter)
                .addContactPoints(prepareContactPoints(contactPointsString))
                .build();

        this.getTaskRunStdout = session.prepare(
                prepareQueryStatement("SELECT log FROM runs.stdout WHERE source = ?")
        );
        this.getTaskRunStderr = session.prepare(
                prepareQueryStatement("SELECT log FROM runs.stderr WHERE source = ?")
        );
        this.insertTask = session.prepare(
                prepareQueryStatement("INSERT INTO runInformation.task (user, name, command, image, submissionTime, " +
                        "memory, cpus, disk ) VALUES (?,?,?,?,?,?,?,?) ")
        );
        this.insertRunTask = session.prepare(
                prepareQueryStatement("INSERT INTO runInformation.taskRun (user, name, runId, submissionTime, state, finishedat) VALUES (?,?,?,?,?,?)")
        );
        this.getLatestRunId = session.prepare(
                prepareQueryStatement("SELECT max(runid) AS maxrunid FROM runInformation.taskRun WHERE user=? AND name=?;")
        );
        this.getTask = session.prepare(
                prepareQueryStatement("SELECT * FROM runInformation.task WHERE user=? and name=?")
        );
        this.getTaskRuns = session.prepare(
                prepareQueryStatement("SELECT * FROM runInformation.taskRUN WHERE user=? AND name=?;")
        );
        this.getTaskRuns = session.prepare(
                prepareQueryStatement("SELECT * FROM runInformation.taskRUN WHERE user=? AND name=?;")
        );
        this.getUserTasks = session.prepare(
          prepareQueryStatement("SELECT * FROM runinformation.task WHERE user=?;")
        );
    }

    private SimpleStatement prepareQueryStatement(String queryString) {
        return SimpleStatement.builder(queryString).setConsistencyLevel(DefaultConsistencyLevel.ONE).build();
    }

    private Collection<InetSocketAddress> prepareContactPoints(String contactPointsString) {
        return Arrays.stream(contactPointsString.split(",")).map(address -> {
            String[] addr = address.split(":");
            return new InetSocketAddress(addr[0], Integer.parseInt(addr[1]));
        }).collect(Collectors.toCollection(LinkedList::new));
    }


    public int nextAvailableRunId(String user, String taskName) {
        BoundStatement boundStatement = getLatestRunId.bind(user, taskName);
        ResultSet rs = session.execute(boundStatement);
        Row row = rs.one();
//        if null cassandra returns 0
        return row.getInt("maxrunid") + 1;
    }

    public Task submitTask(Task task) {
        task.setSubmissionTime(Instant.now());
        BoundStatement boundStatement = insertTask.bind(task.getUser(), task.getName(), task.getCommand(),
                task.getImage(), task.getSubmissionTime().toString(), task.getMemory(), task.getCpus(), task.getDisk());
        session.execute(boundStatement);
        return task;
    }

    public TaskOutput getTaskRunStdout(TaskOutput taskOutput, String taskRunId) {
        taskOutput.setType(TaskOutput.STDOUT);
        BoundStatement boundStatement = getTaskRunStdout.bind(taskRunId);
        ResultSet rs = session.execute(boundStatement);
        taskOutput.setOuput(extractStdResultSet(rs));
        return taskOutput;
    }

    public TaskOutput getTaskRunStderr(TaskOutput taskOutput, String taskRunId) {
        taskOutput.setType(TaskOutput.STDERR);
        BoundStatement boundStatement = getTaskRunStderr.bind(taskRunId);
        ResultSet rs = session.execute(boundStatement);
        taskOutput.setOuput(extractStdResultSet(rs));
        return taskOutput;
    }

    private String[] extractStdResultSet(ResultSet resultSet) {
        return resultSet.all().stream().map((x) -> x.getString("log")).toArray(String[]::new);
    }

    public List<Task> getUserTasks(String user){
        BoundStatement boundStatement = getUserTasks.bind(user);
        ResultSet rs = session.execute(boundStatement);
        return rs.all().stream().map(x -> extractTask(x)).collect(Collectors.toList());
    }
    public Task getTask(String user, String taskName) {

        BoundStatement boundStatement = getTask.bind(user, taskName);
        ResultSet rs = session.execute(boundStatement);
        Row row = rs.one();
        if (row == null) {
            throw new RuntimeException("Task " + taskName + " of user " + user + " does not exist");
        } else {
            return extractTask(row);
        }

    }

    private Task extractTask(Row row){
        Task task = new Task();
        task.setUser(row.getString("user"));
        task.setName(row.getString("name"));
        task.setImage(row.getString("image"));
        task.setCommand(row.getString("command"));
        task.setCpus(row.getFloat("cpus"));
        task.setDisk(row.getInt("disk"));
        task.setMemory(row.getInt("memory"));
        task.setSubmissionTime(Instant.parse(row.getString("submissiontime")));
        return task;
    }

    public TaskRun insertTaskRun(TaskRun taskRun) {
        String taskFinished = taskRun.getFinishedAt()==null?"":taskRun.getFinishedAt().toString();
        BoundStatement boundStatement = insertRunTask.bind(taskRun.getUser(), taskRun.getName(), taskRun.getRunId(),
                taskRun.getSubmissionTime().toString(),taskRun.getState(),taskFinished);
        session.execute(boundStatement);
        return taskRun;
    }

    public List<TaskRun> getTaskRuns(String user, String taskName) {
        BoundStatement boundStatement = getTaskRuns.bind(user,taskName);
        ResultSet rs = session.execute(boundStatement);
        return rs.all().stream().map(x -> {
            TaskRun taskRun = new TaskRun();
            taskRun.setUser(x.getString("user"));
            taskRun.setName(x.getString("name"));
            taskRun.setRunId(x.getInt("runid"));
            taskRun.setState(x.getString("state"));
            taskRun.setSubmissionTime(Instant.parse(x.getString("submissiontime")));
            String finishedAt = x.getString("finishedat");
            if(finishedAt != null && finishedAt != ""){
                taskRun.setFinishedAt(Instant.parse(finishedAt));
            }
            return taskRun;
        }).collect(Collectors.toList());
    }
}
