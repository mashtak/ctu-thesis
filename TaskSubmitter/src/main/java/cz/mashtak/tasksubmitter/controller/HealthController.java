package cz.mashtak.tasksubmitter.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class HealthController {
    @GetMapping("/health")
    @ResponseStatus(value = HttpStatus.OK)
    public void checkHealth(){
        return;
    }
}
