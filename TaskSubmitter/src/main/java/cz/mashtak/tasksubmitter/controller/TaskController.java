package cz.mashtak.tasksubmitter.controller;

import cz.mashtak.tasksubmitter.dto.TaskRun;
import cz.mashtak.tasksubmitter.dto.Task;
import cz.mashtak.tasksubmitter.dto.TaskOutput;
import cz.mashtak.tasksubmitter.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/runs")
public class TaskController {

    @Autowired
    TaskService taskService;

    @GetMapping("/{user}")
    List<Task> getUserTasks(@PathVariable String user) {
        return taskService.getUserTasks(user);
    }

    @PostMapping("/{user}")
    Task addTask(@RequestBody Task task, @PathVariable String user) {
        return taskService.submitTask(user, task);
    }

    @GetMapping("/{user}/{taskName}")
    List<TaskRun> getTaskRuns(@PathVariable String taskName, @PathVariable String user) {
        return taskService.getTaskRuns(user,taskName);
    }

    @PostMapping("/{user}/{taskName}")
    TaskRun runTask(@PathVariable String taskName, @PathVariable String user) {
        return taskService.runTask(user, taskName);
    }

    @GetMapping("/{user}/{taskName}/{runId}/stdout")
    TaskOutput getTaskRunStdout(@PathVariable String taskName, @PathVariable String user, @PathVariable int runId) {
        return taskService.getTaskRunStdout(taskName, user, runId);
    }

    @GetMapping("/{user}/{taskName}/{runId}/stderr")
    TaskOutput getTaskRunStderr(@PathVariable String taskName, @PathVariable String user, @PathVariable int runId) {
        return taskService.getTaskRunStderr(taskName, user, runId);
    }
}
