package cz.mashtak.tasksubmitter.client;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Component
public class MetronomeClient {
    @Value("${metronome.address}")
    private String metronomeUrl;
    private final static String JOBS_API = "/v1/jobs";
    private final static String PROTOCOL = "http://";
    private final static String METRONOME_RUN = "/runs";

    @Autowired
    RestTemplate restTemplate;

    public String checkTaskRunFinished(String id){
        String url = PROTOCOL + metronomeUrl + JOBS_API + "/" + id;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url).queryParam("embed","historySummary");
        log.debug("Quering metronome "+builder.toUriString());
        ResponseEntity<String> result = restTemplate.getForEntity(builder.toUriString(),  String.class);
        log.debug(result.getStatusCode().toString());
        log.debug(result.getBody());
        if(result.getBody() == null){
            return null;
        }
        JsonElement jsonElement = new JsonParser().parse(result.getBody());
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        jsonObject = jsonObject.getAsJsonObject("historySummary");
        log.debug(jsonObject.toString());
        JsonElement successCount = jsonObject.get("successCount");
        if(successCount.getAsInt()>0){
            return jsonObject.get("lastSuccessAt").toString();
        }
        JsonElement failureCount = jsonObject.get("failureCount");
        if(failureCount.getAsInt()>0){
            return jsonObject.get("lastFailureAt").toString();
        }
        return null;
    }

    public void deleteTaskRun(String id){
        String url = PROTOCOL + metronomeUrl + JOBS_API + "/" + id;
        restTemplate.delete(url, null, String.class);
        log.debug("Task "+id+" deleted from Metronome.");
    }

    public void triggerTaskRun(String id) {
        String url = PROTOCOL + metronomeUrl + JOBS_API + "/" + id + METRONOME_RUN;
        log.debug("Sending POST request to url: " + url);
        ResponseEntity<String> result = restTemplate.postForEntity(url, null, String.class);
        log.debug(result.getStatusCode().toString());
        log.debug(result.getBody());
    }

    public void submitTask(JsonObject jsonObject) {
        String url = PROTOCOL + metronomeUrl + JOBS_API;
        log.debug("Sending json to metronome: " + url + "\n" + jsonObject.toString());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(jsonObject.toString(), headers);
        ResponseEntity<String> result = restTemplate.postForEntity(url, entity, String.class);
        log.debug(result.getStatusCode().toString());
        log.debug(result.getBody());
    }
}
