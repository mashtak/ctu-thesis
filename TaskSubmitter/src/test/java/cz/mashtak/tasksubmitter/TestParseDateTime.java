package cz.mashtak.tasksubmitter;

import org.junit.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class TestParseDateTime {
    @Test
    public void testParse(){
        String timestamp = "2019-05-01T19:20:18.836+000";
        timestamp = timestamp.substring(0,timestamp.indexOf("+"));
//        Instant parsedInstant = Instant.parse(timestamp);
        Instant parsedInstant = LocalDateTime.parse(timestamp).toInstant(ZoneOffset.UTC);
        System.out.println(parsedInstant.toString());
    }

}
