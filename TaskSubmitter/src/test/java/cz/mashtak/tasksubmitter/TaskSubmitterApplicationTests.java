package cz.mashtak.tasksubmitter;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskSubmitterApplicationTests {

    @Ignore("Cannot compile if the cassandra not running")
    @Test
    public void contextLoads() {
    }

}
